import React from "react";
import styled from "styled-components";
import DetailCard from "./DetailCard";

const POKE_API = "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/";

let padToThree = (number) => (number < 999 ? `00${number}`.slice(-3) : number);

const openDetailPage = () => <DetailCard />;

const PokeCard = (props) => {
  let imgSrc = `${POKE_API}${padToThree(props.id)}.png`;
  return (
    <Pokecard onClick={() => openDetailPage}>
      <Pokename>{props.name}</Pokename>
      <PokeImg src={imgSrc} alt={props.name} />
      <Pokeprop>Type: {props.type}</Pokeprop>
      <Pokeprop>Exp: {props.exp}</Pokeprop>
    </Pokecard>
  );
};

export default PokeCard;

const Pokecard = styled.div`
  padding: 12px;
  background-color: #2e3440;
  border: none;
  outline: none;
  border-radius: 6px;
  cursor: pointer;
  transition: 0.3s ease-out;
  min-height: 300px;
  min-width: 250px;
  display: flex;
  margin: 20px;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  box-shadow: 15px 22px 15px 0px rgba(0, 0, 0, 0.69);
  -webkit-box-shadow: 15px 22px 15px 0px rgba(0, 0, 0, 0.69);
  -moz-box-shadow: 15px 22px 15px 0px rgba(0, 0, 0, 0.69);

  &:hover {
    transform: translate(-1%, -4%);
    background-color: #434c5e;
  }
`;

const PokeImg = styled.img`
  height: 150px;
  width: 150px;
`;

const Pokename = styled.h2`
  color: hsl(40, 71%, 73%);
  font-size: 2rem;
`;

const Pokeprop = styled.div`
  color: hsl(179, 25%, 65%);
`;
