import React, { useState, useEffect } from "react";
import PokeCard from "./PokeCard";
import styled from "styled-components";

const PokeDex = () => {
  const [pokemon, setPokemon] = useState([]);

  async function f(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() => {
    f(
      "https://raw.githubusercontent.com/fanzeyi/pokemon.json/master/pokedex.json"
    ).then((data) => setPokemon(data));
  }, []);

  return (
    <Pokedex>
      {pokemon.map((p) => (
        <PokeCard
          key={p.id}
          id={p.id}
          name={p.name.english}
          type={p.type[0]}
          exp={p.base.HP}
        />
      ))}
    </Pokedex>
  );
};

export default PokeDex;

const Pokedex = styled.div`
  max-width: 1200px;
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
  margin-bottom: 2rem;
`;
