import PokeDex from "./components/Pokedex";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h1>
        <img
          src="https://see.fontimg.com/api/renderfont4/2ZmK/eyJyIjoiZnMiLCJoIjo3NywidyI6MTAwMCwiZnMiOjc3LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/UG9rZURleA/raphtalia.png"
          alt="Pokedex"
        />
      </h1>
      <PokeDex />
    </div>
  );
}

export default App;
